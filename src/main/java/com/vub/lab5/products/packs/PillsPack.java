package com.vub.lab5.products.packs;

import lombok.Getter;

@Getter
public class PillsPack extends Pack {

    public PillsPack(String name, String serialNumber, int packSize) {
        super(name, serialNumber, packSize);
    }

    @Override
    public String getDescription() {
        return getFullProductName();
    }
}
