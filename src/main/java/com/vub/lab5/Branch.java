package com.vub.lab5;

import com.vub.lab5.products.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@AllArgsConstructor
public class Branch {

    private Company company;
    private String branchName;
    private Location location;
    private Map<Product, Integer> products;
    private boolean closed;

    public Branch(Company company, String branchName, Location location) {
        this.company = company;
        this.branchName = branchName;
        this.location = location;
        this.products = new HashMap<>();
    }

    public Branch(Company company, String branchName, Location location, boolean closed) {
        this.company = company;
        this.branchName = branchName;
        this.location = location;
        this.products = new HashMap<>();
        this.closed = closed;
    }

    public void addProduct(Product product) {
        if (!products.containsKey(product)) {
            products.put(product, 1);
        } else {
            var currentAmount = products.get(product);
            currentAmount++;
            products.put(product, currentAmount);
        }
    }

    public void doStockTaking() {
        var prefix = new StringBuilder()
                .append(branchName)
                .append(Constants.STOCKTAKING_DISPLAY_SEPARATOR)
                .append(location)
                .toString();

        System.out.println(prefix);

        // initialize string builder that will be used to display stock taking
        var sb = new StringBuilder();

        for (Product product: products.keySet()) {
            int productAmount = products.get(product);
            var stockTakeDisplay = sb.append(product.getDescription())
                    .append(": ")
                    .append(productAmount)
                    .toString();
            System.out.println(stockTakeDisplay);

            // clear string builder
            sb.delete(0, sb.length());
        }
        System.out.println("----------------------------------------------");
    }

    public void addProducts(Map<Product, Integer> prods) {
        for (Product prod : prods.keySet()) {
            int amount = prods.get(prod);
            for (int i=0; i<amount; i++) {
                addProduct(prod);
            }
        }
    }

    public void removeProducts() {
        products.clear();
    }

    public void close() {
        closed = true;
    }

}
