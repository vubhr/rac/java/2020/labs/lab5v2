# Lab5

Klijent iz 4. laboratorijske vježbe ima nove zahtjeve.
S obzirom da u prvoj kompaniji unutar grupe jedna poslovnica nije dobro poslovala, klijent ju je odlučio zatvoriti (poslovnicu), a sve proizvode je odlučio prebaciti u drugu poslovnicu unutar te iste kompanije.
Potrebno je:
1. Prvo ispisati stanje skladišta
2. Zatvoriti poslovnicu i sve proizvode prebaciti u drugu poslovnicu
3. Ponovno ispisati stanje skladišta

Zatvaranje poslovnice
1. Sve proizvode prebaciti u novu poslovnicu (to radi kompanija)
2. Implementirati metodu koja će zatvoriti poslovnicu


Uz to, poželjno je koristiti listu umjesto polja.
Rok za predaju je nedjelja **5.4.2020. do 23:59**.